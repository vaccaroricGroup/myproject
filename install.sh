curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
sudo rpm -ivh minikube-latest.x86_64.rpm
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
minikube start
minikube addons enable registry
kubectl create namespace redhat

kubectl config set-context --current --namespace=redhat
kubectl create cm volumes --from-file=vol/
kubectl apply -f deps/web-deployment.yaml
kubectl apply -f deps/sftp-deployment.yaml
kubectl expose deployment sftp --type ClusterIP --port 22
kubectl expose deployment web --type ClusterIP --port 80
kamel install
kamel run Integration.java --dev


