This project is an home assignement. In order to install all the needed component the installation.sh script needs to be executed.

Right below, the assignement:

Get access to a kubernetes cluster. You can install minikube, k3s, CodeReadyContainers or use a cloud provider based cluster.
Install the latest release of Camel-K into the cluster.
Write and deploy a simple Camel-K integration that will periodically poll data from a web service and an SFTP server. You can decide on the details of what the integration actually does with the data, how/whether you combine the two sources etc.
Both the web server and the SFTP server will run in the same kubernetes cluster as Camel-K. Write kubernetes resource files that will deploy each of the servers, including all the necessary resources (services, configmaps etc). Make sure that the address of the web server is configurable without changing the integration's source code.
The "service" provided by the web server can be just a static JSON file, but do not bundle it into the image, provide it using a kubernetes volume.
Provide the solution as a git repo with all the necessary files, with an entry shell script that will install and deploy everything.
