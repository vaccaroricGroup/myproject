import org.apache.camel.builder.RouteBuilder;

public class Integration extends RouteBuilder {
  @Override
  public void configure() throws Exception {

//      from("timer:java?period=1000")
//        .setHeader("example")
//         .constant("Java")
//       .setBody()
//       .simple("Hello World! Camel K route written in ${header.example}.")
//       .to("log:info");

	from("timer:java?period=5000")
	.to("http://web")
	.log("Response from the web server: ${body}");

	from("sftp:sftp:22/upload?username=riccardo&password=mypass&allowNullBody=true&fileName=response.json&noop=true")
  	.to("log:info")
  	.log("This is the return from the sftp: ${body}");

  }


}
